'use strict';

import fs from 'fs'
import path from 'path'
import mjmlEngine from 'mjml'
import browserSync from 'browser-sync'
import open from 'open'
import merge from 'merge-stream'
import gulp from 'gulp'
import mjml from 'gulp-mjml'
import plumber from 'gulp-plumber'
import imagemin from 'gulp-imagemin'
import sizereport from 'gulp-sizereport'
import zip from 'gulp-zip'
import index from 'gulp-index'
import replace from 'gulp-replace'
import ftp from 'vinyl-ftp'

// preview server details
const server = {
    projectName: 'Branded Offer',
    baseFolder: 'FCA/CRM',
    domain: 'preview.publicis.ca',
    host: '167.246.44.216',
    user: 'previewftp',
    password: 'ce5aw&thuTuS'
}

// analytics snippets
const gaSnippet = '<img src = "https://www.google-analytics.com/collect?v=1&tid=UA-1341196-19&cid=555&aip=1&t=event&ec=email&ea=open&dp=%2Femail&dt=BRNDOFFR" />'
const litmusSnippet = `<style data-ignore-inlining>@media print{ #_t { background-image: url('https://bayd32iy.emltrk.com/bayd32iy?p&d=%%Email%%');}} div.OutlookMessageHeader {background-image:url('https://bayd32iy.emltrk.com/bayd32iy?f&d=%%Email%%')} table.moz-email-headers-table {background-image:url('https://bayd32iy.emltrk.com/bayd32iy?f&d=%%Email%%')} blockquote #_t {background-image:url('https://bayd32iy.emltrk.com/bayd32iy?f&d=%%Email%%')} #MailContainerBody #_t {background-image:url('https://bayd32iy.emltrk.com/bayd32iy?f&d=%%Email%%')}</style><div id="_t"></div>
<img src="https://bayd32iy.emltrk.com/bayd32iy?d=%%Email%%" width="1" height="1" border="0" alt="" />`

// index page options
const indexOpts = {
    'relativePath': './src',
    'prepend-to-output': () => `<head> <style>body{background: #D52536; background-image: url(pub.svg); background-repeat: no-repeat; background-position: center; background-size: auto 70%; font-family: system-ui; color: #fff; font-weight: 500; font-size: 16px; padding: 40px; line-height: 1em;}a{color: #fff;}</style></head><body>`,
    'title': server.projectName,
    'section-heading-template': () => '',
    'item-template': (filepath, filename) => `<li class="index__item"><a class="index__item-link" href="${filepath}/${filename}">${filepath}</a></li>`
}

const getFolders = (dir) => {
    return fs.readdirSync(dir)
        .filter((file) => {
            return fs.statSync(path.join(dir, file)).isDirectory()
        })
}

const compileFolder = (folder) => {
    return function () {
        return gulp.src(`src/${folder}/*.mjml`)
            .pipe(plumber())
            .pipe(mjml(mjmlEngine, { minify: true, keepComments: false, validationLevel: 'strict' }))
            .pipe(replace('</body>', `${litmusSnippet}${gaSnippet}</body>`))
            .pipe(gulp.dest(`./src/${folder}`))
    }
}

const compileAll = () => {
    let comp = getFolders('src').map((folder) => {
        return compileFolder(folder)()
    })
    return merge(comp)
}

const list = () => {
    return gulp.src('./src/*/*.html')
        .pipe(sizereport({ '*': { 'maxSize': 80000 } }))
        .pipe(index(indexOpts))
        .pipe(gulp.dest('./src'))
}

const reload = (done) => {
    browserSync.reload()
    done()
}

const watchFiles = () => {
    browserSync.init({ server: 'src', baseDir: './src' })

    gulp.watch(['src/*', '!src/index.html'], gulp.series(list, reload))
    getFolders('src').forEach((folder) => {
        gulp.watch(`src/${folder}/*.mjml`, gulp.series(compileFolder(folder), reload))
    })
}

const images = () => {
    return gulp.src('src/**/*.+(jpg|png|gif)')
        .pipe(imagemin({ verbose: false }))
        .pipe(gulp.dest((file) => {
            return file.base
        }))
}

const compress = () => {
    let zipFiles = getFolders('src').map((folder) => {
        return gulp.src(`src/${folder}/**/*.+(html|jpg|png|gif)`)
            .pipe(zip(`${folder}.zip`))
            .pipe(gulp.dest('dist'))
    })
    let zipSize = gulp.src('./dist/*')
        .pipe(sizereport({ '*': { 'maxSize': 1500000 } }))
    return merge(zipFiles, zipSize)
}

const test = () => {
    let html = getFolders('src').map((folder) => {
        return gulp.src(path.join('src', folder, '*.html'))
            .pipe(replace('src="img/', `src="http://${server.domain}/${server.baseFolder}/${server.projectName}/${folder}/img/`))
            .pipe(gulp.dest(`./test/${folder}`))
    })
    return merge(html)
}

const previewUpload = () => {
    const conn = ftp.create({ host: server.host, user: server.user, password: server.password })
    let indexUpload = gulp.src(['./src/index.html', './src/pub.svg'])
        .pipe(conn.dest(`${server.baseFolder}/${server.projectName}/`))
    let htmlUpload = getFolders('src').map((folder) => {
        return gulp.src(path.join('src', folder, '*.html'))
            .pipe(conn.dest(`${server.baseFolder}/${server.projectName}/${folder}`))
    })
    let imgUpload = getFolders('src').map((folder) => {
        return gulp.src(`./src/${folder}/img/*`)
            .pipe(conn.dest(`${server.baseFolder}/${server.projectName}/${folder}/img`))
    })
    return merge(indexUpload, htmlUpload, imgUpload)
}

const previewOpen = (done) => {
    open(`http://${server.domain}/${server.baseFolder}/${server.projectName}`)
    done()
}

const compile = gulp.series(compileAll, list)
const watch = gulp.series(compileAll, list, watchFiles)
const preview = gulp.series(gulp.parallel(compileAll, images), list, previewUpload, previewOpen)
const build = gulp.series(gulp.parallel(compileAll, images), list, compress)

export { compile, watch, images, test, preview, build }
export default watch